const express = require('express');
const cors = require('cors');
const passport = require('passport');
const UserRouter = require('./router/UserRouter');
const PublicRouter = require('./router/PublicRouter');
const LocalStrategy = require('./strategies/LocalStrategy');
const JwtStrategy = require('./strategies/JwtStrategy');

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());
app.use(passport.initialize());

passport.use('local', LocalStrategy);
passport.use('jwt', JwtStrategy);

app.use(express.static('public'));

app.use('/api/user', UserRouter);
app.use('/api', PublicRouter);

app.listen(3000, err => {
    if(err) {
        console.error(err);
        return;
    }
    console.log('Webserver running on port 3000');
});
