const express = require('express');
const passport = require('passport');
const router = express.Router();

router.use(passport.authenticate('jwt', {session: false}));

router.get('/', (req, res) => {
    return res.json(req.user);
});

module.exports = router;
